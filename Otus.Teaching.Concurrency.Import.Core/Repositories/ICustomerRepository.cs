using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetAll();
        void Add/*Customer*/(Customer customer);
        Customer Find(int Id);
        Task<IEnumerable<Customer>> GetAllAsync();
        Task AddAsync(Customer customer);
        Task <Customer> FindAsync(int Id);
    }
}