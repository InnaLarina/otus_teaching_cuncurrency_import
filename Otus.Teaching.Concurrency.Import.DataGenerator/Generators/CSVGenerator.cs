﻿using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CSVGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CSVGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using (StreamWriter sw = new StreamWriter(_fileName, false, System.Text.Encoding.Default))
            {
                foreach (Customer customer in customers)
                {
                    sw.WriteLineAsync($"{customer.Id}; {customer.FullName}; {customer.Email}; {customer.Phone};");
                }
            }
        }

    }
}
