﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        public CustomerController(ICustomerRepository customerRepository)
        {
            CustomerRepository = customerRepository;
        }
        public ICustomerRepository CustomerRepository { get; set; }
        public IEnumerable<Customer> GetAll()
        {
            return CustomerRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetCustomer")]
        public IActionResult GetById(int Id)
        {
            Customer customer = CustomerRepository.Find(Id);

            if (customer == null)
            {
                return NotFound();
            }
            return new ObjectResult(customer);
        }
        [HttpPost]
        public IActionResult Create([FromBody] Customer customer)
        {
            if (customer == null)
            {
                return BadRequest();
            }
            CustomerRepository.Add(customer);
            return CreatedAtRoute("GetCustomer", new { Id = customer.Id }, customer);
        }
        
    }
}
