﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.Drawing;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public static class IEnumerableExtention
    {
        private static int _treadCount = 5;
        private static Customer[] arrCustomers;
        private static string log_file = @"D:\C#Education\Projects\9report.txt";
        public static void CallThreads(this IEnumerable<Customer> customers, int modelUsing = 1)
        {
            using (StreamWriter f = new StreamWriter(log_file, true)) 
            { 
              Stopwatch stopwath = new Stopwatch();
              stopwath.Start();
              arrCustomers = customers.Cast<Customer>().ToArray();
              int maxId = arrCustomers.Max(x => (int)x.Id);//количество записей клиентов
              int range = maxId / _treadCount;
              for (int p1 = 0; p1 < _treadCount; p1++)
              {
                int p = p1;
                int start = p * range;
                int end = (p == _treadCount - 1) ? maxId : start + range;
                switch (modelUsing)
                {
                    case 1:
                        ThreadPool.QueueUserWorkItem(PrintArrCustomerThreadingPool, new Point(start, end));
                        break;
                    case 2:
                        ParamFunc pf = new ParamFunc(start, end);
                        Thread thread = new Thread(new ThreadStart(pf.PrintArrCustomer));
                        thread.Start();
                        break;
                    default:
                        break;
                }
              }
              stopwath.Stop();
              TimeSpan ts = stopwath.Elapsed;
              string text = $"Затрачено {ts.TotalSeconds} секунд";
              Console.WriteLine(text);
              f.WriteLine(text);
            }

            void PrintArrCustomerThreadingPool(Object o)
            {
              if (o is Point)
              {
                Point p = (Point)o;
                int start = p.X;
                int end = p.Y;
                Console.WriteLine($"Текущий поток N {Thread.CurrentThread.ManagedThreadId}");

                for (int i = start; i < end; i++)
                {
                    Thread.Sleep(10);
                }
                Console.WriteLine($"Текущий поток N {Thread.CurrentThread.ManagedThreadId} отработал");
              }
            }
        }
        class ParamFunc
        {
            private int start;
            private int end;

            public ParamFunc(int _start, int _end)
            {
                this.start = _start;
                this.end = _end;
            }

            public void PrintArrCustomer()
            {
                Console.WriteLine($"Текущий поток N {Thread.CurrentThread.ManagedThreadId}");

                for (int i = start; i < end; i++)
                {
                    Thread.Sleep(10);
                }
                Console.WriteLine($"Текущий поток N {Thread.CurrentThread.ManagedThreadId} отработал");
            }

        } 
    }
    

}

