﻿using System;
using System.Diagnostics;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Handler.Entities;



namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = @"D:\C#Education\Projects\13.xml";
        private static int _trial = 3;
        static void Main(string[] args)
        {
            if (args != null && args.Length == 1)
            {
                _dataFilePath = args[0];
            }
            else
            {
                Console.WriteLine("DataFilePath is required");
            }
            
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");
            for (int i = 0; i < _trial; i++)
            {
                try
                {
                    var loader = new FakeDataLoaderThreadPool(_dataFilePath);
                    loader.LoadData();
                    return;
                }
                catch (Exception ex) 
                {
                    Console.WriteLine(ex.Message+" Устраните ошибку и нашмите любую клавишу.");
                    Console.ReadKey();
                }
            }
        }
    }
}