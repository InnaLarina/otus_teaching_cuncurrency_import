using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository
        : ICustomerRepository
    {
        DbContextOptions<CustomerContext> optionsBuilder = new DbContextOptions<CustomerContext>();
        CustomerContext customerContext;
        public CustomerRepository()
        {
            customerContext = new CustomerContext(optionsBuilder);
        }

        public IEnumerable<Customer> GetAll()
        {
            return customerContext.Customers;
        }

        public void Add(Customer customer) 
        {
            customerContext.Customers.Add(customer);
            customerContext.SaveChanges();
        }

        public Customer Find(int Id)
        {
            Customer foundCustomer = customerContext.Customers.Find(Id);
            return foundCustomer;
        }

        public async Task<IEnumerable<Customer>> GetAllAsync()
        {
            return await customerContext.Customers.ToListAsync();
        }
        public async Task AddAsync(Customer customer)
        {
            await customerContext.AddAsync(customer);
            await customerContext.SaveChangesAsync();
        }
        public async Task<Customer> FindAsync(int Id)
        {
           return await customerContext.Customers.FindAsync(Id);
        }
    }
}