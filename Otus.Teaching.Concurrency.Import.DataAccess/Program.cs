﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    class Program
    {
        private static string _workingDirectory = @"D:\C#Education\Projects";
        private static string _fileName = "7.xml";
        static void Main(string[] args)
        {
            string fileName = Path.Combine(_workingDirectory, _fileName);
            XElement users = XElement.Load(fileName);
            int maxId = users.Descendants("Customer").Max(x => (int)x.Element("Id"));
            Console.WriteLine(maxId.ToString());
            Console.ReadKey();
        }
    }
}
