﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using WebAPI.Controllers;
using Xunit;

namespace XUnitTests
{
    public class CustomerControllerTests
    {
        private CustomerController customerController;

        public CustomerControllerTests()
        {
            var repository = new CustomerRepository();
            customerController = new CustomerController(repository);
        }
        [Fact]
        public void GetCustomerByIdWithStatusCodeOk()
        {
            var result = customerController.GetById(2);
            Assert.IsType<ObjectResult>(result);
        }
        [Fact]
        public void GetCustomerByIdWithStatusCodeNotFound()
        {
            var result = customerController.GetById(2000);
            Assert.IsType<NotFoundResult>(result);
        }
        [Fact]
        public void AddCustomerWithStatusCodeOK()
        {
            var customer = new Customer()
            {
                Phone = "79120000001",
                FullName = "Emma",
                Email = "Emma1996@gmail.com"
            };

            var result = customerController.Create(customer);
            Assert.IsType<CreatedAtRouteResult>(result);
        }
        [Fact]
        public void AddCustomerWithStatusBadRequest()
        {
            Customer customer = null;
            var result = customerController.Create(customer);
            Assert.IsType<BadRequestResult>(result);
        }
    }
}
