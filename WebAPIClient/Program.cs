﻿using Bogus;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebAPIClient
{
    class Program
    {
        static HttpClient client = new HttpClient();
        private const string APP_PATH = "https://localhost:44341/";
        //private const string APP_PATH = "https://localhost:44394/";
        static async Task Main(string[] args)
        {
            await RunAsync();
        }

        static Faker<Customer> CreateFaker()
        {
            var id = 1;
            var customersFaker = new Faker<Customer>("ru")
                .CustomInstantiator(f => new Customer()
                {
                    Id = id++
                })
                .RuleFor(u => u.FullName, (f, u) => f.Name.FullName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FullName))
                .RuleFor(u => u.Phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            return customersFaker;
        }
        static async Task RunAsync()
        {
            //Генерация случайного клиента
            Customer fakeCustomer = CreateFaker();
            Customer customer = new Customer { FullName = fakeCustomer.FullName, Email = fakeCustomer.Email, Phone = fakeCustomer.Phone };
            //Добавление случайного клиента
            client.BaseAddress = new Uri(APP_PATH);
            var result = await client.PostAsJsonAsync("api/customer", customer);
            Console.WriteLine("Введите Id Клиента");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                result = await client.GetAsync("api/customer/" + id);
                var str = result.Content.ReadAsStringAsync().ConfigureAwait(false);
                string str_result = str.GetAwaiter().GetResult();
                if (str_result == "null")
                    Console.WriteLine("Нет клиента с таким Id");
                else
                    Console.WriteLine("Клиент: " + str_result);
            }
            catch (OverflowException)
            {
                Console.WriteLine("Число вне диапазона целого числа.");
            }
            catch (FormatException)
            {
                Console.WriteLine("Введено не целое число.");
            }

            Console.ReadKey();
        }
    }
}
